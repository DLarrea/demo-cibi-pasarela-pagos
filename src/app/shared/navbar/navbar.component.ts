import { Component, OnInit } from '@angular/core';
import { PlubService } from 'src/app/services/plub.service';
import { User } from '../../utils/User';
import {MenuItem} from 'primeng/api';
import { AppStateService } from 'src/app/services/app-state.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  currentUser!: User;
  items: MenuItem[];

  constructor(private plubService: PlubService, private appState: AppStateService, private router: Router) {
    this.items = [
      {
        label: 'Medios de pago',
        icon: 'pi pi-credit-card',
        command: () => {
          this.router.navigate([`user/medios-pagos`])
        }
      }
    ]
  }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers = () => {
    this.plubService.getUsers().subscribe(data => {
      this.currentUser = data[this.selectUser(0,0)];
      this.appState.set(this.currentUser);
    })
  }

  selectUser = (min:number, max:number) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}

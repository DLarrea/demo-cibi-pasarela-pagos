import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './inicio/inicio.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { PrimengModule } from './primeng/primeng.module';
import { DecimalPipe } from '@angular/common';
import { MediosPagosComponent } from './medios-pagos/medios-pagos.component';
import { SingleBuyComponent } from './single-buy/single-buy.component';
import { MediosPagosAgregarComponent } from './medios-pagos/medios-pagos-agregar/medios-pagos-agregar.component';
import { MediosPagosAgregarReturnComponent } from './medios-pagos/medios-pagos-agregar-return/medios-pagos-agregar-return.component';
import { SingleBuyReturnComponent } from './single-buy/single-buy-return/single-buy-return.component';
import { SingleBuyCancelComponent } from './single-buy/single-buy-cancel/single-buy-cancel.component';
import { FormsModule } from '@angular/forms';
import { MessageService } from 'primeng/api';
@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    NavbarComponent,
    MediosPagosComponent,
    SingleBuyComponent,
    MediosPagosAgregarComponent,
    MediosPagosAgregarReturnComponent,
    SingleBuyReturnComponent,
    SingleBuyCancelComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    PrimengModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [DecimalPipe, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }

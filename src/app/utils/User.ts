export interface User {
    email: string;
    name: string;
    phone: string;
    user_id: number;
}
export interface Card {
    alias_token: string;
    card_masked_number: string;
    expiration_date:string;
    card_brand: string;
    card_id: number;
    card_type: string;
    default: boolean;
};
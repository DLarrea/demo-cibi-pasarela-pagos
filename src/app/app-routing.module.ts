import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { MediosPagosAgregarReturnComponent } from './medios-pagos/medios-pagos-agregar-return/medios-pagos-agregar-return.component';
import { MediosPagosAgregarComponent } from './medios-pagos/medios-pagos-agregar/medios-pagos-agregar.component';
import { MediosPagosComponent } from './medios-pagos/medios-pagos.component';
import { SingleBuyCancelComponent } from './single-buy/single-buy-cancel/single-buy-cancel.component';
import { SingleBuyReturnComponent } from './single-buy/single-buy-return/single-buy-return.component';
import { SingleBuyComponent } from './single-buy/single-buy.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'plub',
    pathMatch: 'full'
  },
  {
    path: 'plub',
    component: InicioComponent
  },
  {
    path: 'user/medios-pagos',
    component: MediosPagosComponent
  },
  {
    path: 'user/medios-pagos/agregar',
    component: MediosPagosAgregarComponent
  },
  {
    path: 'user/medios-pagos/return',
    component: MediosPagosAgregarReturnComponent
  },
  {
    path: 'product/:product/single-buy',
    component: SingleBuyComponent
  },
  {
    path: 'product/:product/single-buy/return',
    component: SingleBuyReturnComponent
  },
  {
    path: 'product/:product/single-buy/cancel',
    component: SingleBuyCancelComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }

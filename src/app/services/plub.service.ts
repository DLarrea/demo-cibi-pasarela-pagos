import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../utils/Product';
import { User } from '../utils/User';
@Injectable({
  providedIn: 'root'
})
export class PlubService {

  constructor(private http: HttpClient) {}


  getUsers = ():Observable<User[]> => {
    return this.http.get<User[]>('assets/users.json');
  }
  
  getProducts = ():Observable<Product[]> => {
    return this.http.get<Product[]>('assets/products.json');
  }
}

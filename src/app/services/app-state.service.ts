import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../utils/User';

@Injectable({
  providedIn: 'root'
})
export class AppStateService {

  constructor() { }

  private userSource = new BehaviorSubject<User>(null as any);
  public user = this.userSource.asObservable();

  public get = () => {
    return this.userSource.getValue();
  }

  public set = (user:User) => {
    this.userSource.next(user);
  }
}

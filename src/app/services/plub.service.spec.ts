import { TestBed } from '@angular/core/testing';

import { PlubService } from './plub.service';

describe('PlubService', () => {
  let service: PlubService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlubService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

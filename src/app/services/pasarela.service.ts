import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PasarelaResponse } from '../utils/PasarelaResponse';
@Injectable({
  providedIn: 'root'
})
export class PasarelaService {

  constructor(private http: HttpClient) { }

  getProccessId = (form:any):Observable<any> => {
    return this.http.post<any>(`${environment.pasarelaUrl}/bancard/single-buy`, form);
  }
  
  getCardProccessId = (form:any):Observable<any> => {
    return this.http.post<any>(`${environment.pasarelaUrl}/bancard/cards-new`, form);
  }
  
  getUserCads = (idUser:number):Observable<PasarelaResponse | undefined> => {
    return this.http.get<PasarelaResponse | undefined>(`${environment.pasarelaUrl}/bancard/retrieve-user-cards/${idUser}`);
  }
  
  deleteUserCard = (form:any, idUser:number):Observable<PasarelaResponse | undefined> => {
    return this.http.delete<PasarelaResponse | undefined>(`${environment.pasarelaUrl}/bancard/delete-user-cards/${idUser}`, {body:form});
  }
  
  confirmOrRejectCard = (form:any):Observable<any> => {
    return this.http.post<any>(`${environment.pasarelaUrl}/bancard/confirm-or-reject-card`, form);
  }
  
  cardAsDefault = (form:any):Observable<any> => {
    return this.http.post<any>(`${environment.pasarelaUrl}/bancard/card-as-default`, form);
  }

  tokenPayment = (form:any):Observable<any> => {
    return this.http.post<any>(`${environment.pasarelaUrl}/bancard/token-payment`, form);
  }

}

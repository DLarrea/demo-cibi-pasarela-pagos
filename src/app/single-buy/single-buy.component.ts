import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { environment } from 'src/environments/environment';
import { AppStateService } from '../services/app-state.service';
import { PasarelaService } from '../services/pasarela.service';
import { PlubService } from '../services/plub.service';
import { Card } from '../utils/Card';
import { Product } from '../utils/Product';
import { User } from '../utils/User';

declare var Bancard:any;
@Component({
  selector: 'app-single-buy',
  templateUrl: './single-buy.component.html',
  styleUrls: ['./single-buy.component.scss']
})
export class SingleBuyComponent implements OnInit {

  defaultCard!: Card;
  productId: number = 0;
  ordenDeCompra: number = this.randomIntFromInterval(1,999);
  product!: Product;
  currentUser!: User;
  cuotas = [2,3,4,5,6,7,8,9,10,11,12];
  cuotaSeleccionada = null;
  styles = {
    "form-background-color": "#001b60",
    "button-background-color": "#4faed1",
    "button-text-color": "#fcfcfc",
    "button-border-color": "#dddddd",
    "input-background-color": "#fcfcfc",
    "input-text-color": "#111111",
    "input-placeholder-color": "#111111"
  };
  constructor(private route: ActivatedRoute, private plubService: PlubService, private pasarelaService: PasarelaService, 
    private appState: AppStateService, private msg: MessageService) {
    this.appState.user.subscribe(
      data => {
        this.currentUser = data;
        if(this.currentUser) this.getMediosPagos(this.currentUser.user_id);
      }
    )
    
    this.route.paramMap.subscribe(params => {
      this.productId = Number(params.get('product'));
      this.getProduct();
    });
  }

  ngOnInit(): void {
  }

  getProduct = () => {
    this.plubService.getProducts().subscribe(
      data => {
        this.product = <Product>data.find(it => it.id === this.productId);
        this.getProcessId();
      }
    )
  }

  getProcessId = () => {
    let bancardForm = {
      shop_process_id: this.ordenDeCompra, 
      amount: this.product.price,
      return_url: `${environment.demoUrl}/#/product/${this.product.id}/single-buy/return`,
    };
    this.pasarelaService.getProccessId(bancardForm).subscribe(
      response => {
        Bancard.Checkout.createForm ('iframe-container', response.data.process_id, this.styles);
      }
    )
  }

  getMediosPagos = (idUser:number) => {
    this.pasarelaService.getUserCads(idUser).subscribe(
      response => {
        if(response) {
          this.defaultCard = response.data?.cards?.find((el:Card) => {
            return el.default;
          });
        } 
      },
      error => {
        console.log(error);
      }
    )
  }

  tokenPayment = () => {
    let form = {
      shopProcessId: this.ordenDeCompra,
      amount: this.product.price,
      number_of_payments: this.cuotaSeleccionada == null ? 1: this.cuotaSeleccionada,
      alias_token: this.defaultCard.alias_token,
      card_id: this.defaultCard.card_id
    }
    this.pasarelaService.tokenPayment(form).subscribe(
      response => {
        this.msg.add({severity:'info', summary:'Compra de producto', detail: response.data.message});
      },
      err => {
        this.msg.add({severity:'info', summary:'Compra de producto', detail:'Error al procesar la solicitud'});
      }
    )
  }

  randomIntFromInterval(min:number, max:number) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
  }
}

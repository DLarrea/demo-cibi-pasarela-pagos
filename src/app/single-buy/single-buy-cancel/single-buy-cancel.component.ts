import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';

@Component({
  selector: 'app-single-buy-cancel',
  templateUrl: './single-buy-cancel.component.html',
  styleUrls: ['./single-buy-cancel.component.scss']
})
export class SingleBuyCancelComponent implements OnInit {

  msgs!: Message[];
  constructor() {
    this.msgs = [{severity:'info', summary:'Compra de producto', detail: 'La compra ha sido cancelada', closable:false}];
  }

  ngOnInit(): void {}

}

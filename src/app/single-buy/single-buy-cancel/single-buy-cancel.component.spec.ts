import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleBuyCancelComponent } from './single-buy-cancel.component';

describe('SingleBuyCancelComponent', () => {
  let component: SingleBuyCancelComponent;
  let fixture: ComponentFixture<SingleBuyCancelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleBuyCancelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleBuyCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Message } from 'primeng/api';
import { AppStateService } from 'src/app/services/app-state.service';
import { User } from 'src/app/utils/User';

@Component({
  selector: 'app-single-buy-return',
  templateUrl: './single-buy-return.component.html',
  styleUrls: ['./single-buy-return.component.scss']
})
export class SingleBuyReturnComponent implements OnInit {

  currentUser!: User;
  status: string = '';
  description: string = '';

  msgs!: Message[];
  constructor(private route: ActivatedRoute, private appState: AppStateService) {
    this.appState.user.subscribe(
      data => {
        this.currentUser = data;
      }
    )
    this.route.queryParams.subscribe(params => {
      this.status = params['status'];
      this.description = params['description'];

      if(this.status == 'payment_success'){
        this.msgs = [{severity:'info', summary:'Compra de producto', detail: 'Su compra se ha realizado correctamente', closable:false}];
      } else {
        this.msgs = [{severity:'info', summary:'Error realizar la compra', detail: this.description, closable:false}];
      }
      
    });

  }

  ngOnInit(): void {
  }

}

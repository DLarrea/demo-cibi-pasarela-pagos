import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleBuyReturnComponent } from './single-buy-return.component';

describe('SingleBuyReturnComponent', () => {
  let component: SingleBuyReturnComponent;
  let fixture: ComponentFixture<SingleBuyReturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleBuyReturnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleBuyReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

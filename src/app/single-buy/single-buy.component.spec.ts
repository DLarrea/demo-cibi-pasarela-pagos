import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleBuyComponent } from './single-buy.component';

describe('SingleBuyComponent', () => {
  let component: SingleBuyComponent;
  let fixture: ComponentFixture<SingleBuyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleBuyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

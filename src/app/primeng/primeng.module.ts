import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {AvatarModule} from 'primeng/avatar';
import {CardModule} from 'primeng/card';
import {DividerModule} from 'primeng/divider';
import {ButtonModule} from 'primeng/button';
import {MenuModule} from 'primeng/menu';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {MessagesModule} from 'primeng/messages';
import { TagModule } from 'primeng/tag';
import {DropdownModule} from 'primeng/dropdown';
import {ToastModule} from 'primeng/toast';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    AvatarModule,
    CardModule,
    DividerModule,
    ButtonModule,
    MenuModule,
    BreadcrumbModule,
    MessagesModule,
    TagModule,
    DropdownModule,
    ToastModule
  ]
})
export class PrimengModule { }

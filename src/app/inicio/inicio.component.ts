import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { PlubService } from '../services/plub.service';
import { Product } from '../utils/Product';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  productos!: Product[];
  bItems: MenuItem[];
  bHome: MenuItem = {
    icon: 'pi pi-home'
  }
  constructor(private plubService: PlubService) {
    this.bItems = [
      {label:'Productos'}
    ];
  }

  ngOnInit(): void {
    this.getProductos();
  }

  getProductos = () => {
    this.plubService.getProducts().subscribe(
      data => {
        this.productos = data;
      }
    )
  }

}

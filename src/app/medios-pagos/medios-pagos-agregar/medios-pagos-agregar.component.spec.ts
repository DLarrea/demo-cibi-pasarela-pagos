import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MediosPagosAgregarComponent } from './medios-pagos-agregar.component';

describe('MediosPagosAgregarComponent', () => {
  let component: MediosPagosAgregarComponent;
  let fixture: ComponentFixture<MediosPagosAgregarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MediosPagosAgregarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MediosPagosAgregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

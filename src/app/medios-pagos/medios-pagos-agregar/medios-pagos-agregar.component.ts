import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AppStateService } from 'src/app/services/app-state.service';
import { PasarelaService } from 'src/app/services/pasarela.service';
import { User } from 'src/app/utils/User';
import { environment } from 'src/environments/environment';

declare var Bancard:any;
@Component({
  selector: 'app-medios-pagos-agregar',
  templateUrl: './medios-pagos-agregar.component.html',
  styleUrls: ['./medios-pagos-agregar.component.scss']
})
export class MediosPagosAgregarComponent implements OnInit {

  currentUser!: User;
  bItems!: MenuItem[];
  bHome: MenuItem = {
    icon: 'pi pi-home',
    routerLink: '/plub'
  }
  styles = {
    "form-background-color": "#001b60",
    "button-background-color": "#4faed1",
    "button-text-color": "#fcfcfc",
    "button-border-color": "#dddddd",
    "input-background-color": "#fcfcfc",
    "input-text-color": "#111111",
    "input-placeholder-color": "#111111"
  };
  constructor(private appState: AppStateService, private pasarelaService: PasarelaService) {
    this.appState.user.subscribe(
      data => {
        this.currentUser = data;
        if(this.currentUser){
          this.bItems = [
            {label: this.currentUser.name},
            {label: 'Medios de pago'},
            {label: 'Agregar medio de pago'}
          ];
          this.getCardProccessId();
        }
      }
    )
  }

  ngOnInit(): void {
  }

  getCardProccessId = () => {
    let bancardForm = {
      card_id: new Date().getTime(),
      user_id: this.currentUser.user_id,
      user_cell_phone: this.currentUser.phone,
      user_mail: this.currentUser.email,
      return_url: `${environment.demoUrl}/#/user/medios-pagos/return`
    };

    localStorage.setItem('card_id', bancardForm.card_id.toString());

    this.pasarelaService.getCardProccessId(bancardForm).subscribe(
      response => {
        Bancard.Cards.createForm('iframe-container',response.data.process_id, this.styles);
      }
    )
  }
}

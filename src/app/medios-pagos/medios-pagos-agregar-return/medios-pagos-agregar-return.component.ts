import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Message } from 'primeng/api';
import { AppStateService } from 'src/app/services/app-state.service';
import { PasarelaService } from 'src/app/services/pasarela.service';
import { User } from 'src/app/utils/User';

@Component({
  selector: 'app-medios-pagos-agregar-return',
  templateUrl: './medios-pagos-agregar-return.component.html',
  styleUrls: ['./medios-pagos-agregar-return.component.scss']
})
export class MediosPagosAgregarReturnComponent implements OnInit {

  currentUser!: User;
  status: string = '';
  description: string = '';

  msgs!: Message[];
  constructor(private route: ActivatedRoute, private appState: AppStateService, private pasarelaService: PasarelaService) {
    this.appState.user.subscribe(
      data => {
        this.currentUser = data;
      }
    )
    this.route.queryParams.subscribe(params => {
      this.status = params['status'];
      this.description = params['description'];

      if(this.status == 'add_new_card_success'){
        this.msgs = [{severity:'info', summary:'Catastro de tarjeta', detail: 'Su tarjeta ha sido registrada exitosamente', closable:false}];
      } else {
        this.msgs = [{severity:'info', summary:'Error al catastrar tarjeta', detail: this.description, closable:false}];
      }
      
      this.confirmOrReject(this.status, this.currentUser.user_id, <number>Number(localStorage.getItem('card_id')));
    });

  }

  ngOnInit(): void {
  }

  confirmOrReject = (status: string, user_id: number, card_id: number) => {
    this.pasarelaService.confirmOrRejectCard({status, user_id, card_id}).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log(error);
      }
    )
  }
}

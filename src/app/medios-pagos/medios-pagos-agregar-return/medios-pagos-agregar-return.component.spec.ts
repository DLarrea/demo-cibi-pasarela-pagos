import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MediosPagosAgregarReturnComponent } from './medios-pagos-agregar-return.component';

describe('MediosPagosAgregarReturnComponent', () => {
  let component: MediosPagosAgregarReturnComponent;
  let fixture: ComponentFixture<MediosPagosAgregarReturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MediosPagosAgregarReturnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MediosPagosAgregarReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

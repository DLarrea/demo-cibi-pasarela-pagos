import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AppStateService } from '../services/app-state.service';
import { PasarelaService } from '../services/pasarela.service';
import { Card } from '../utils/Card';
import { User } from '../utils/User';

@Component({
  selector: 'app-medios-pagos',
  templateUrl: './medios-pagos.component.html',
  styleUrls: ['./medios-pagos.component.scss']
})
export class MediosPagosComponent implements OnInit {

  currentUser!: User;
  bItems!: MenuItem[];
  bHome: MenuItem = {
    icon: 'pi pi-home',
    routerLink: '/plub'
  }
  currentUserCards!: Card[];

  constructor(private appState: AppStateService, private pasarelaService: PasarelaService) {
    this.appState.user.subscribe(
      data => {
        this.currentUser = data;
        if(this.currentUser){
          this.bItems = [
            {label: this.currentUser.name},
            {label: 'Medios de pago'}
          ];
          this.getMediosPagos(this.currentUser.user_id);
        }
      }
    )
  }

  ngOnInit(): void {
  }

  getMediosPagos = (idUser:number) => {
    this.pasarelaService.getUserCads(idUser).subscribe(
      response => {
        if(response) this.currentUserCards = response.data.cards;
        else this.currentUserCards = [];
      },
      error => {
        this.currentUserCards = [];
      }
    )
  }

  deleteCard = (alias_token:string, card_id: number) => {
    let form = {
      alias_token,
      card_id
    }
    this.pasarelaService.deleteUserCard(form, this.currentUser.user_id).subscribe(
      data => {
        this.getMediosPagos(this.currentUser.user_id);
      }
    )
  }

  cardAsDefault = (card_id:number) => {
    this.pasarelaService.cardAsDefault({card_id:card_id, user_id:this.currentUser.user_id}).subscribe(
      data => {
        this.getMediosPagos(this.currentUser.user_id);
      }
    )
  }
}
